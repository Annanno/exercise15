package com.example.exercise15

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.map


class DataStoreManager(private val context:Context) {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "userInfo")


    companion object {

        val firstName = stringPreferencesKey("firstName")
        val lastName = stringPreferencesKey("lastName")
        val address = stringPreferencesKey("address")
        val age = stringPreferencesKey("age")
        val phoneNumber = stringPreferencesKey("phoneNumber")
        val email = stringPreferencesKey("email")
        val gender = stringPreferencesKey("gender")
        val imageUrl = stringPreferencesKey("imageUrl")


    }

    suspend fun saveData(userInfo: User) {
        context.dataStore.edit {

            it[firstName] = userInfo.firstName
            it[lastName] = userInfo.lastName
            it[address] = userInfo.address
            it[age] = userInfo.age
            it[phoneNumber] = userInfo.phoneNumber
            it[email] = userInfo.email
            it[gender] = userInfo.gender
            it[imageUrl] = userInfo.imageUrl

        }
    }
    fun readFromDataStore() = context.dataStore.data.map {
        User(
            firstName = it[firstName]?:"Jemali",
            lastName = it[lastName]?:"kakauridze",
            age = it[age]?:"100",
            email = it[email]?:"jemali@gmail.com",
            gender = it[gender]?:"male",
            imageUrl = it[imageUrl]?:"https://images.ctfassets.net/81iqaqpfd8fy/3r4flvP8Z26WmkMwAEWEco/870554ed7577541c5f3bc04942a47b95/78745131.jpg?w=1200&h=1200&fm=jpg&fit=fill",
            phoneNumber = it[phoneNumber]?:"000000",
            address = it[address]?:""
        )
    }
}