package com.example.exercise15


data class User(val firstName: String, val lastName: String, val age: String,
                val address: String, val phoneNumber: String, val email : String,
                val gender: String, val imageUrl: String)