package com.example.exercise15.fragments


import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.exercise15.DataStoreManager
import com.example.exercise15.R
import com.example.exercise15.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    override fun init() {
        binding.editProfileBtn.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch() {
                withContext(IO) {
                    dataStoreManager.readFromDataStore().catch {

                    }.collect {
                        withContext(Dispatchers.Main) {
                            binding.firstName.text = it.firstName
                            binding.lastName.text = it.lastName
                            binding.address.text = it.address
                            binding.age.text = it.age
                            binding.email.text = it.email
                            binding.gender.text = it.gender

                        }


                    }
                }
            }
            it.findNavController().navigate(R.id.action_profileFragment_to_editProfileFragment)

        }


    }
}