package com.example.exercise15.fragments


import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.example.exercise15.DataStoreManager
import com.example.exercise15.R
import com.example.exercise15.User
import com.example.exercise15.databinding.FragmentEditProfileBinding
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class EditProfileFragment : BaseFragment<FragmentEditProfileBinding>(FragmentEditProfileBinding::inflate) {


    override fun init() {
        binding.editBtn.setOnClickListener{
            viewLifecycleOwner.lifecycleScope.launch {
                withContext(IO){
                    dataStoreManager.saveData(userInfo = User(
                        firstName = binding.firstName.text.toString(),
                        lastName = binding.lastName.text.toString(),
                        email = binding.email.text.toString(),
                        address = binding.address.text.toString(),
                        age = binding.age.text.toString(),
                        gender = binding.gender.text.toString(),
                        imageUrl = binding.profileIV.text.toString(),
                        phoneNumber = binding.phone.text.toString()
                    ))
                }

            }
            it.findNavController().navigate(R.id.action_editProfileFragment_to_profileFragment)
        }
    }

}